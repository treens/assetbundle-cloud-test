# Swagger\Client\BuildtargetsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBuildTarget**](BuildtargetsApi.md#addBuildTarget) | **POST** /orgs/{orgid}/projects/{projectid}/buildtargets | Create build target for a project
[**deleteBuildTarget**](BuildtargetsApi.md#deleteBuildTarget) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid} | Delete build target
[**getBuildTarget**](BuildtargetsApi.md#getBuildTarget) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid} | Get a build target
[**getBuildTargets**](BuildtargetsApi.md#getBuildTargets) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets | List all build targets for a project
[**getBuildTargetsForOrg**](BuildtargetsApi.md#getBuildTargetsForOrg) | **GET** /orgs/{orgid}/buildtargets | List all build targets for an org
[**updateBuildTarget**](BuildtargetsApi.md#updateBuildTarget) | **PUT** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid} | Update build target details


# **addBuildTarget**
> \Swagger\Client\Model\InlineResponse20012 addBuildTarget($orgid, $projectid, $options)

Create build target for a project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$options = new \Swagger\Client\Model\Options8(); // \Swagger\Client\Model\Options8 | Options for build target create/update

try {
    $result = $api_instance->addBuildTarget($orgid, $projectid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->addBuildTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **options** | [**\Swagger\Client\Model\Options8**](../Model/\Swagger\Client\Model\Options8.md)| Options for build target create/update |

### Return type

[**\Swagger\Client\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBuildTarget**
> string deleteBuildTarget($orgid, $projectid, $buildtargetid)

Delete build target

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name

try {
    $result = $api_instance->deleteBuildTarget($orgid, $projectid, $buildtargetid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->deleteBuildTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuildTarget**
> \Swagger\Client\Model\InlineResponse20012 getBuildTarget($orgid, $projectid, $buildtargetid)

Get a build target

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name

try {
    $result = $api_instance->getBuildTarget($orgid, $projectid, $buildtargetid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->getBuildTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |

### Return type

[**\Swagger\Client\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuildTargets**
> \Swagger\Client\Model\InlineResponse20012[] getBuildTargets($orgid, $projectid, $include, $include_last_success)

List all build targets for a project

Gets all configured build targets for a project, regardless of whether they are enabled. Add \"?include=settings,credentials\" as a query parameter to include the build target settings and credentials with the response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$include = "include_example"; // string | Extra fields to include in the response
$include_last_success = false; // bool | Include last successful build

try {
    $result = $api_instance->getBuildTargets($orgid, $projectid, $include, $include_last_success);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->getBuildTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **include** | **string**| Extra fields to include in the response | [optional]
 **include_last_success** | **bool**| Include last successful build | [optional] [default to false]

### Return type

[**\Swagger\Client\Model\InlineResponse20012[]**](../Model/InlineResponse20012.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuildTargetsForOrg**
> \Swagger\Client\Model\InlineResponse20012[] getBuildTargetsForOrg($orgid, $include, $include_last_success)

List all build targets for an org

Gets all configured build targets for an org, regardless of whether they are enabled. Add \"?include=settings,credentials\" as a query parameter to include the build target settings and credentials with the response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$include = "include_example"; // string | Extra fields to include in the response
$include_last_success = false; // bool | Include last successful build

try {
    $result = $api_instance->getBuildTargetsForOrg($orgid, $include, $include_last_success);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->getBuildTargetsForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **include** | **string**| Extra fields to include in the response | [optional]
 **include_last_success** | **bool**| Include last successful build | [optional] [default to false]

### Return type

[**\Swagger\Client\Model\InlineResponse20012[]**](../Model/InlineResponse20012.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBuildTarget**
> \Swagger\Client\Model\InlineResponse20012 updateBuildTarget($orgid, $projectid, $buildtargetid, $options)

Update build target details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildtargetsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$options = new \Swagger\Client\Model\Options9(); // \Swagger\Client\Model\Options9 | Options for build target create/update

try {
    $result = $api_instance->updateBuildTarget($orgid, $projectid, $buildtargetid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildtargetsApi->updateBuildTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **options** | [**\Swagger\Client\Model\Options9**](../Model/\Swagger\Client\Model\Options9.md)| Options for build target create/update |

### Return type

[**\Swagger\Client\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

