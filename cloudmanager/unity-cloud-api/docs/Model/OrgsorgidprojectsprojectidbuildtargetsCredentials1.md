# OrgsorgidprojectsprojectidbuildtargetsCredentials1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**signing** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentials1Signing**](OrgsorgidprojectsprojectidbuildtargetsCredentials1Signing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


