# InlineResponse2006BillingDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_feature_name** | **string** |  | [optional] 
**start_date** | **string** |  | [optional] 
**end_date** | **string** |  | [optional] 
**last_amount_paid** | **float** |  | [optional] 
**currency** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


