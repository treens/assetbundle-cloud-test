# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**projectid** | **string** |  | [optional] 
**org_name** | **string** |  | [optional] 
**orgid** | **string** |  | [optional] 
**guid** | **string** |  | [optional] 
**created** | **string** |  | [optional] 
**cached_icon** | **string** |  | [optional] 
**settings** | [**\Swagger\Client\Model\ProjectsSettings**](ProjectsSettings.md) |  | [optional] 
**service_flags** | **object** |  | [optional] 
**links** | **object** |  | [optional] 
**disabled** | **bool** |  | [optional] 
**disable_notifications** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


