# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**udid** | **string** |  | 
**devicename** | **string** |  | [optional] 
**os** | **string** |  | [optional] 
**osversion** | **string** |  | [optional] 
**product** | **string** |  | [optional] 
**status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


