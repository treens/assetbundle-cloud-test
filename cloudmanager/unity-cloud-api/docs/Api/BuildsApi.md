# Swagger\Client\BuildsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**batchDeleteBuildArtifacts**](BuildsApi.md#batchDeleteBuildArtifacts) | **POST** /orgs/{orgid}/projects/{projectid}/artifacts/delete | Delete artifacts for a batch of builds
[**cancelAllBuilds**](BuildsApi.md#cancelAllBuilds) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds | Cancel all builds
[**cancelBuild**](BuildsApi.md#cancelBuild) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number} | Cancel build
[**cancelBuildsForOrg**](BuildsApi.md#cancelBuildsForOrg) | **DELETE** /orgs/{orgid}/builds | Cancel builds for org
[**createShare**](BuildsApi.md#createShare) | **POST** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/share | Create a new link to share a project
[**deleteAllBuildArtifacts**](BuildsApi.md#deleteAllBuildArtifacts) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/artifacts | Delete all artifacts associated with all non-favorited builds for a specified buildtargetid (_all is allowed).
[**deleteBuildArtifacts**](BuildsApi.md#deleteBuildArtifacts) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/artifacts | Delete all artifacts associated with a specific build
[**getAuditLog2**](BuildsApi.md#getAuditLog2) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/auditlog | Get audit log
[**getAuditLog3**](BuildsApi.md#getAuditLog3) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/auditlog | Get audit log
[**getBuild**](BuildsApi.md#getBuild) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number} | Build Status
[**getBuildLog**](BuildsApi.md#getBuildLog) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/log | Get build log
[**getBuilds**](BuildsApi.md#getBuilds) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds | List all builds
[**getBuildsForOrg**](BuildsApi.md#getBuildsForOrg) | **GET** /orgs/{orgid}/builds | List all builds for org
[**getShare**](BuildsApi.md#getShare) | **GET** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/share | Get the share link
[**revokeShare**](BuildsApi.md#revokeShare) | **DELETE** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number}/share | Revoke a shared link
[**startBuilds**](BuildsApi.md#startBuilds) | **POST** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds | Create new build
[**updateBuild**](BuildsApi.md#updateBuild) | **PUT** /orgs/{orgid}/projects/{projectid}/buildtargets/{buildtargetid}/builds/{number} | Update build information


# **batchDeleteBuildArtifacts**
> string batchDeleteBuildArtifacts($orgid, $projectid, $options)

Delete artifacts for a batch of builds

Delete all artifacts associated with the builds identified by the provided build target ids and build numbers. Builds marked as do not delete or that are currently building will be ignored.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$options = new \Swagger\Client\Model\Options10(); // \Swagger\Client\Model\Options10 | Options to specify what builds to delete

try {
    $result = $api_instance->batchDeleteBuildArtifacts($orgid, $projectid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->batchDeleteBuildArtifacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **options** | [**\Swagger\Client\Model\Options10**](../Model/\Swagger\Client\Model\Options10.md)| Options to specify what builds to delete |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelAllBuilds**
> string cancelAllBuilds($orgid, $projectid, $buildtargetid)

Cancel all builds

Cancel all builds in progress for this build target (or all targets, if '_all' is specified as the buildtargetid). Canceling an already finished build will do nothing and respond successfully.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name

try {
    $result = $api_instance->cancelAllBuilds($orgid, $projectid, $buildtargetid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->cancelAllBuilds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelBuild**
> string cancelBuild($orgid, $projectid, $buildtargetid, $number)

Cancel build

Cancel a build in progress. Canceling an already finished build will do nothing and respond successfully.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all

try {
    $result = $api_instance->cancelBuild($orgid, $projectid, $buildtargetid, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->cancelBuild: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelBuildsForOrg**
> string cancelBuildsForOrg($orgid)

Cancel builds for org

Cancel all in progress builds for an organization. Canceling an already finished build will do nothing and respond successfully.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier

try {
    $result = $api_instance->cancelBuildsForOrg($orgid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->cancelBuildsForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createShare**
> \Swagger\Client\Model\InlineResponse20015 createShare($orgid, $projectid, $buildtargetid, $number)

Create a new link to share a project

Create a new short link to share a project. If this is called when a share already exists, that share will be revoked and a new one created.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all

try {
    $result = $api_instance->createShare($orgid, $projectid, $buildtargetid, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->createShare: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |

### Return type

[**\Swagger\Client\Model\InlineResponse20015**](../Model/InlineResponse20015.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllBuildArtifacts**
> string deleteAllBuildArtifacts($orgid, $projectid, $buildtargetid)

Delete all artifacts associated with all non-favorited builds for a specified buildtargetid (_all is allowed).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name

try {
    $result = $api_instance->deleteAllBuildArtifacts($orgid, $projectid, $buildtargetid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->deleteAllBuildArtifacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBuildArtifacts**
> string deleteBuildArtifacts($orgid, $projectid, $buildtargetid, $number)

Delete all artifacts associated with a specific build

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all

try {
    $result = $api_instance->deleteBuildArtifacts($orgid, $projectid, $buildtargetid, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->deleteBuildArtifacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAuditLog2**
> \Swagger\Client\Model\InlineResponse20011[] getAuditLog2($orgid, $projectid, $buildtargetid, $per_page, $page)

Get audit log

Retrieve a list of historical settings changes for this build target.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$per_page = 25; // float | Number of audit log records to retrieve
$page = 1; // float | Skip to page number, based on per_page value

try {
    $result = $api_instance->getAuditLog2($orgid, $projectid, $buildtargetid, $per_page, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getAuditLog2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **per_page** | **float**| Number of audit log records to retrieve | [optional] [default to 25]
 **page** | **float**| Skip to page number, based on per_page value | [optional] [default to 1]

### Return type

[**\Swagger\Client\Model\InlineResponse20011[]**](../Model/InlineResponse20011.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAuditLog3**
> \Swagger\Client\Model\InlineResponse20011[] getAuditLog3($orgid, $projectid, $buildtargetid, $number, $per_page, $page)

Get audit log

Retrieve a list of settings changes between the last and current build.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all
$per_page = 25; // float | Number of audit log records to retrieve
$page = 1; // float | Skip to page number, based on per_page value

try {
    $result = $api_instance->getAuditLog3($orgid, $projectid, $buildtargetid, $number, $per_page, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getAuditLog3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |
 **per_page** | **float**| Number of audit log records to retrieve | [optional] [default to 25]
 **page** | **float**| Skip to page number, based on per_page value | [optional] [default to 1]

### Return type

[**\Swagger\Client\Model\InlineResponse20011[]**](../Model/InlineResponse20011.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuild**
> \Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds getBuild($orgid, $projectid, $buildtargetid, $number, $include)

Build Status

Retrieve information for a specific build. A Build resource contains information related to a build attempt for a build target, including the build number, changeset, build times, and other pertinent data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all
$include = "include_example"; // string | Extra fields to include in the response

try {
    $result = $api_instance->getBuild($orgid, $projectid, $buildtargetid, $number, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getBuild: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |
 **include** | **string**| Extra fields to include in the response | [optional]

### Return type

[**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds**](../Model/OrgsorgidprojectsprojectidbuildtargetsBuilds.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuildLog**
> getBuildLog($orgid, $projectid, $buildtargetid, $number, $offsetlines, $linenumbers, $compact, $with_html)

Get build log

Retrieve the plain text log for a specifc build.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all
$offsetlines = 1; // float | Stream log from the given line number
$linenumbers = false; // bool | Include log line numbers in the text output
$compact = false; // bool | Return the compact log, showing only errors and warnings
$with_html = false; // bool | Surround important lines (errors, warnings) with SPAN and CSS markup

try {
    $api_instance->getBuildLog($orgid, $projectid, $buildtargetid, $number, $offsetlines, $linenumbers, $compact, $with_html);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getBuildLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |
 **offsetlines** | **float**| Stream log from the given line number | [optional] [default to 1]
 **linenumbers** | **bool**| Include log line numbers in the text output | [optional] [default to false]
 **compact** | **bool**| Return the compact log, showing only errors and warnings | [optional] [default to false]
 **with_html** | **bool**| Surround important lines (errors, warnings) with SPAN and CSS markup | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuilds**
> \Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[] getBuilds($orgid, $projectid, $buildtargetid, $include, $per_page, $page, $build_status, $platform, $show_deleted, $only_favorites)

List all builds

List all running and finished builds, sorted by build number (optionally paginating the results). Use '_all' as the buildtargetid to get all configured build targets. The response includes a Content-Range header that identifies the range of results returned and the total number of results matching the given query parameters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$include = "include_example"; // string | Extra fields to include in the response
$per_page = 25; // float | Number of audit log records to retrieve
$page = 1; // float | Skip to page number, based on per_page value
$build_status = ""; // string | Query for only builds of a specific status
$platform = ""; // string | Query for only builds of a specific platform
$show_deleted = false; // bool | Query for builds that have been deleted
$only_favorites = false; // bool | Query for builds that have been favorited

try {
    $result = $api_instance->getBuilds($orgid, $projectid, $buildtargetid, $include, $per_page, $page, $build_status, $platform, $show_deleted, $only_favorites);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getBuilds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **include** | **string**| Extra fields to include in the response | [optional]
 **per_page** | **float**| Number of audit log records to retrieve | [optional] [default to 25]
 **page** | **float**| Skip to page number, based on per_page value | [optional] [default to 1]
 **build_status** | **string**| Query for only builds of a specific status | [optional] [default to ]
 **platform** | **string**| Query for only builds of a specific platform | [optional] [default to ]
 **show_deleted** | **bool**| Query for builds that have been deleted | [optional] [default to false]
 **only_favorites** | **bool**| Query for builds that have been favorited | [optional] [default to false]

### Return type

[**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[]**](../Model/OrgsorgidprojectsprojectidbuildtargetsBuilds.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBuildsForOrg**
> \Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[] getBuildsForOrg($orgid, $include, $per_page, $page, $build_status, $platform, $show_deleted, $only_favorites)

List all builds for org

List all running and finished builds, sorted by build number (optionally paginating the results). The response includes a Content-Range header that identifies the range of results returned and the total number of results matching the given query parameters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$include = "include_example"; // string | Extra fields to include in the response
$per_page = 25; // float | Number of audit log records to retrieve
$page = 1; // float | Skip to page number, based on per_page value
$build_status = ""; // string | Query for only builds of a specific status
$platform = ""; // string | Query for only builds of a specific platform
$show_deleted = false; // bool | Query for builds that have been deleted
$only_favorites = false; // bool | Query for builds that have been favorited

try {
    $result = $api_instance->getBuildsForOrg($orgid, $include, $per_page, $page, $build_status, $platform, $show_deleted, $only_favorites);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getBuildsForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **include** | **string**| Extra fields to include in the response | [optional]
 **per_page** | **float**| Number of audit log records to retrieve | [optional] [default to 25]
 **page** | **float**| Skip to page number, based on per_page value | [optional] [default to 1]
 **build_status** | **string**| Query for only builds of a specific status | [optional] [default to ]
 **platform** | **string**| Query for only builds of a specific platform | [optional] [default to ]
 **show_deleted** | **bool**| Query for builds that have been deleted | [optional] [default to false]
 **only_favorites** | **bool**| Query for builds that have been favorited | [optional] [default to false]

### Return type

[**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[]**](../Model/OrgsorgidprojectsprojectidbuildtargetsBuilds.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getShare**
> \Swagger\Client\Model\InlineResponse20015 getShare($orgid, $projectid, $buildtargetid, $number)

Get the share link

Gets a share link if it exists

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all

try {
    $result = $api_instance->getShare($orgid, $projectid, $buildtargetid, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->getShare: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |

### Return type

[**\Swagger\Client\Model\InlineResponse20015**](../Model/InlineResponse20015.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **revokeShare**
> string revokeShare($orgid, $projectid, $buildtargetid, $number)

Revoke a shared link

Revoke a shared link, both {buildtargetid} and {number} may use _all to revoke all share links for a given buildtarget or entire project.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all

try {
    $result = $api_instance->revokeShare($orgid, $projectid, $buildtargetid, $number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->revokeShare: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **startBuilds**
> \Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[] startBuilds($orgid, $projectid, $buildtargetid, $options)

Create new build

Start the build process for this build target (or all targets, if '_all' is specified as the buildtargetid), if there is not one currently in process.  If a build is currently in process that information will be related in the 'error' field.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$options = new \Swagger\Client\Model\Options11(); // \Swagger\Client\Model\Options11 | Options for starting the builds

try {
    $result = $api_instance->startBuilds($orgid, $projectid, $buildtargetid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->startBuilds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **options** | [**\Swagger\Client\Model\Options11**](../Model/\Swagger\Client\Model\Options11.md)| Options for starting the builds | [optional]

### Return type

[**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[]**](../Model/OrgsorgidprojectsprojectidbuildtargetsBuilds.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBuild**
> \Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds updateBuild($orgid, $projectid, $buildtargetid, $number, $options)

Update build information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\BuildsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$buildtargetid = "buildtargetid_example"; // string | unique id auto-generated from the build target name
$number = "number_example"; // string | Build number or in some cases _all
$options = new \Swagger\Client\Model\Options12(); // \Swagger\Client\Model\Options12 | Options for build update

try {
    $result = $api_instance->updateBuild($orgid, $projectid, $buildtargetid, $number, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuildsApi->updateBuild: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **buildtargetid** | **string**| unique id auto-generated from the build target name |
 **number** | **string**| Build number or in some cases _all |
 **options** | [**\Swagger\Client\Model\Options12**](../Model/\Swagger\Client\Model\Options12.md)| Options for build update |

### Return type

[**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds**](../Model/OrgsorgidprojectsprojectidbuildtargetsBuilds.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

