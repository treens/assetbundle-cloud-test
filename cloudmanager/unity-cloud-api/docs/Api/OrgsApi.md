# Swagger\Client\OrgsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBillingPlans1**](OrgsApi.md#getBillingPlans1) | **GET** /orgs/{orgid}/billingplan | Get billing plan
[**getSSHKey1**](OrgsApi.md#getSSHKey1) | **GET** /orgs/{orgid}/sshkey | Get SSH Key
[**regenerateSSHKey**](OrgsApi.md#regenerateSSHKey) | **POST** /orgs/{orgid}/sshkey | Regenerate SSH Key


# **getBillingPlans1**
> \Swagger\Client\Model\InlineResponse2006 getBillingPlans1($orgid)

Get billing plan

Get the billing plan for the specified organization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\OrgsApi();
$orgid = "orgid_example"; // string | Organization identifier

try {
    $result = $api_instance->getBillingPlans1($orgid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrgsApi->getBillingPlans1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSSHKey1**
> \Swagger\Client\Model\InlineResponse2008 getSSHKey1($orgid)

Get SSH Key

Get the ssh public key for the specified org

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\OrgsApi();
$orgid = "orgid_example"; // string | Organization identifier

try {
    $result = $api_instance->getSSHKey1($orgid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrgsApi->getSSHKey1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **regenerateSSHKey**
> \Swagger\Client\Model\InlineResponse2008 regenerateSSHKey($orgid)

Regenerate SSH Key

Regenerate the ssh key for the specified org *WARNING* this is a destructive operation that will permanently remove your current SSH key.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\OrgsApi();
$orgid = "orgid_example"; // string | Organization identifier

try {
    $result = $api_instance->regenerateSSHKey($orgid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrgsApi->regenerateSSHKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

