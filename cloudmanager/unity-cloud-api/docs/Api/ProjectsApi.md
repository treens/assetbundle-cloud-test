# Swagger\Client\ProjectsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addProject**](ProjectsApi.md#addProject) | **POST** /orgs/{orgid}/projects | Create project
[**archiveProject**](ProjectsApi.md#archiveProject) | **DELETE** /orgs/{orgid}/projects/{projectid} | Archive project
[**getAuditLog1**](ProjectsApi.md#getAuditLog1) | **GET** /orgs/{orgid}/projects/{projectid}/auditlog | Get audit log
[**getBillingPlans2**](ProjectsApi.md#getBillingPlans2) | **GET** /orgs/{orgid}/projects/{projectid}/billingplan | Get billing plan
[**getProject**](ProjectsApi.md#getProject) | **GET** /orgs/{orgid}/projects/{projectid} | Get project details
[**getProjectByUpid**](ProjectsApi.md#getProjectByUpid) | **GET** /projects/{projectupid} | Get project details
[**getSSHKey2**](ProjectsApi.md#getSSHKey2) | **GET** /orgs/{orgid}/projects/{projectid}/sshkey | Get SSH Key
[**getStats**](ProjectsApi.md#getStats) | **GET** /orgs/{orgid}/projects/{projectid}/stats | Get project statistics
[**listProjectsForOrg**](ProjectsApi.md#listProjectsForOrg) | **GET** /orgs/{orgid}/projects | List all projects (org)
[**listProjectsForUser**](ProjectsApi.md#listProjectsForUser) | **GET** /projects | List all projects (user)
[**updateProject**](ProjectsApi.md#updateProject) | **PUT** /orgs/{orgid}/projects/{projectid} | Update project details


# **addProject**
> \Swagger\Client\Model\InlineResponse2009 addProject($orgid, $options)

Create project

Create a project for the specified organization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$options = new \Swagger\Client\Model\Options4(); // \Swagger\Client\Model\Options4 | Options for project create/update

try {
    $result = $api_instance->addProject($orgid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->addProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **options** | [**\Swagger\Client\Model\Options4**](../Model/\Swagger\Client\Model\Options4.md)| Options for project create/update |

### Return type

[**\Swagger\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **archiveProject**
> string archiveProject($orgid, $projectid)

Archive project

This will archive the project in Cloud Build ONLY. Use with caution - this process is not reversible. The projects UPID will be removed from Cloud Build allowing the project to be reconfigured.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->archiveProject($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->archiveProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAuditLog1**
> \Swagger\Client\Model\InlineResponse20011[] getAuditLog1($orgid, $projectid, $per_page, $page)

Get audit log

Retrieve a list of historical settings changes for this project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$per_page = 25; // float | Number of audit log records to retrieve
$page = 1; // float | Skip to page number, based on per_page value

try {
    $result = $api_instance->getAuditLog1($orgid, $projectid, $per_page, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getAuditLog1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **per_page** | **float**| Number of audit log records to retrieve | [optional] [default to 25]
 **page** | **float**| Skip to page number, based on per_page value | [optional] [default to 1]

### Return type

[**\Swagger\Client\Model\InlineResponse20011[]**](../Model/InlineResponse20011.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBillingPlans2**
> \Swagger\Client\Model\InlineResponse2006 getBillingPlans2($orgid, $projectid)

Get billing plan

Get the billing plan for the specified project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->getBillingPlans2($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getBillingPlans2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProject**
> \Swagger\Client\Model\InlineResponse2009 getProject($orgid, $projectid, $include)

Get project details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$include = "include_example"; // string | Extra fields to include in the response

try {
    $result = $api_instance->getProject($orgid, $projectid, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **include** | **string**| Extra fields to include in the response | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProjectByUpid**
> \Swagger\Client\Model\InlineResponse2009 getProjectByUpid($projectupid)

Get project details

Gets the same data as /orgs/{orgid}/project/{projectid} but looks up the project by the Unity Project ID. This value is returned in the project's guid field.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$projectupid = "projectupid_example"; // string | Project UPID - Unity global id

try {
    $result = $api_instance->getProjectByUpid($projectupid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getProjectByUpid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectupid** | **string**| Project UPID - Unity global id |

### Return type

[**\Swagger\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSSHKey2**
> \Swagger\Client\Model\InlineResponse2008 getSSHKey2($orgid, $projectid)

Get SSH Key

Get the ssh public key for the specified project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->getSSHKey2($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getSSHKey2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStats**
> \Swagger\Client\Model\InlineResponse20010 getStats($orgid, $projectid)

Get project statistics

Get statistics for the specified project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->getStats($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->getStats: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse20010**](../Model/InlineResponse20010.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProjectsForOrg**
> \Swagger\Client\Model\InlineResponse2009[] listProjectsForOrg($orgid, $include)

List all projects (org)

List all projects that belong to the specified organization. Add \"?include=settings\" as a query parameter to include the project settings with the response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$include = "include_example"; // string | Extra fields to include in the response

try {
    $result = $api_instance->listProjectsForOrg($orgid, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->listProjectsForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **include** | **string**| Extra fields to include in the response | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2009[]**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProjectsForUser**
> \Swagger\Client\Model\InlineResponse2009[] listProjectsForUser($include)

List all projects (user)

List all projects that you have permission to access across all organizations. Add \"?include=settings\" as a query parameter to include the project settings with the response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$include = "include_example"; // string | Extra fields to include in the response

try {
    $result = $api_instance->listProjectsForUser($include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->listProjectsForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include** | **string**| Extra fields to include in the response | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2009[]**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProject**
> \Swagger\Client\Model\InlineResponse2009 updateProject($orgid, $projectid, $options)

Update project details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\ProjectsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$options = new \Swagger\Client\Model\Options5(); // \Swagger\Client\Model\Options5 | Options for project create/update

try {
    $result = $api_instance->updateProject($orgid, $projectid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->updateProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **options** | [**\Swagger\Client\Model\Options5**](../Model/\Swagger\Client\Model\Options5.md)| Options for project create/update |

### Return type

[**\Swagger\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

