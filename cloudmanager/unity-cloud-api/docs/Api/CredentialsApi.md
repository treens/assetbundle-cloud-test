# Swagger\Client\CredentialsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCredentialsAndroid**](CredentialsApi.md#addCredentialsAndroid) | **POST** /orgs/{orgid}/projects/{projectid}/credentials/signing/android | Upload Android Credentials
[**addCredentialsIos**](CredentialsApi.md#addCredentialsIos) | **POST** /orgs/{orgid}/projects/{projectid}/credentials/signing/ios | Upload iOS Credentials
[**deleteAndroid**](CredentialsApi.md#deleteAndroid) | **DELETE** /orgs/{orgid}/projects/{projectid}/credentials/signing/android/{credentialid} | Delete Android Credentials
[**deleteIos**](CredentialsApi.md#deleteIos) | **DELETE** /orgs/{orgid}/projects/{projectid}/credentials/signing/ios/{credentialid} | Delete iOS Credentials
[**getAllAndroid**](CredentialsApi.md#getAllAndroid) | **GET** /orgs/{orgid}/projects/{projectid}/credentials/signing/android | Get All Android Credentials
[**getAllIos**](CredentialsApi.md#getAllIos) | **GET** /orgs/{orgid}/projects/{projectid}/credentials/signing/ios | Get All iOS Credentials
[**getOneAndroid**](CredentialsApi.md#getOneAndroid) | **GET** /orgs/{orgid}/projects/{projectid}/credentials/signing/android/{credentialid} | Get Android Credential Details
[**getOneIos**](CredentialsApi.md#getOneIos) | **GET** /orgs/{orgid}/projects/{projectid}/credentials/signing/ios/{credentialid} | Get iOS Credential Details
[**updateAndroid**](CredentialsApi.md#updateAndroid) | **PUT** /orgs/{orgid}/projects/{projectid}/credentials/signing/android/{credentialid} | Update Android Credentials
[**updateIos**](CredentialsApi.md#updateIos) | **PUT** /orgs/{orgid}/projects/{projectid}/credentials/signing/ios/{credentialid} | Update iOS Credentials


# **addCredentialsAndroid**
> \Swagger\Client\Model\InlineResponse20013 addCredentialsAndroid($orgid, $projectid, $label, $file, $alias, $keypass, $storepass)

Upload Android Credentials

Upload a new android keystore for the project. NOTE: you must be a manager in the project's organization to add new credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$label = "label_example"; // string | Label for the uploaded credential
$file = "/path/to/file.txt"; // \SplFileObject | Keystore file
$alias = "alias_example"; // string | Keystore alias
$keypass = "keypass_example"; // string | Keystore keypass
$storepass = "storepass_example"; // string | Keystore storepass

try {
    $result = $api_instance->addCredentialsAndroid($orgid, $projectid, $label, $file, $alias, $keypass, $storepass);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->addCredentialsAndroid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **label** | **string**| Label for the uploaded credential |
 **file** | **\SplFileObject**| Keystore file |
 **alias** | **string**| Keystore alias |
 **keypass** | **string**| Keystore keypass |
 **storepass** | **string**| Keystore storepass |

### Return type

[**\Swagger\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addCredentialsIos**
> \Swagger\Client\Model\InlineResponse20014 addCredentialsIos($orgid, $projectid, $label, $file_certificate, $file_provisioning_profile, $certificate_pass)

Upload iOS Credentials

Upload a new iOS certificate and provisioning profile for the project. NOTE: you must be a manager in the project's organization to add new credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$label = "label_example"; // string | Label for the uploaded credentials
$file_certificate = "/path/to/file.txt"; // \SplFileObject | Certificate file (.p12)
$file_provisioning_profile = "/path/to/file.txt"; // \SplFileObject | Provisioning Profile (.mobileprovision)
$certificate_pass = "certificate_pass_example"; // string | Certificate (.p12) password

try {
    $result = $api_instance->addCredentialsIos($orgid, $projectid, $label, $file_certificate, $file_provisioning_profile, $certificate_pass);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->addCredentialsIos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **label** | **string**| Label for the uploaded credentials |
 **file_certificate** | **\SplFileObject**| Certificate file (.p12) |
 **file_provisioning_profile** | **\SplFileObject**| Provisioning Profile (.mobileprovision) |
 **certificate_pass** | **string**| Certificate (.p12) password | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAndroid**
> string deleteAndroid($orgid, $projectid, $credentialid)

Delete Android Credentials

Delete specific android credentials for a project. NOTE: you must be a manager in the project's organization to delete credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier

try {
    $result = $api_instance->deleteAndroid($orgid, $projectid, $credentialid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->deleteAndroid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteIos**
> string deleteIos($orgid, $projectid, $credentialid)

Delete iOS Credentials

Delete specific ios credentials for a project. NOTE: you must be a manager in the project's organization to delete credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier

try {
    $result = $api_instance->deleteIos($orgid, $projectid, $credentialid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->deleteIos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllAndroid**
> \Swagger\Client\Model\InlineResponse20013[] getAllAndroid($orgid, $projectid)

Get All Android Credentials

Get all credentials available for the project. A user in the projects org will see all credentials uploaded for any project within the org, whereas a user with project-level permissions will only see credentials assigned to the specific project.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->getAllAndroid($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->getAllAndroid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse20013[]**](../Model/InlineResponse20013.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllIos**
> \Swagger\Client\Model\InlineResponse20014[] getAllIos($orgid, $projectid)

Get All iOS Credentials

Get all credentials available for the project. A user in the projects org will see all credentials uploaded for any project within the org, whereas a user with project-level permissions will only see credentials assigned to the specific project.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->getAllIos($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->getAllIos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse20014[]**](../Model/InlineResponse20014.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOneAndroid**
> \Swagger\Client\Model\InlineResponse20013 getOneAndroid($orgid, $projectid, $credentialid)

Get Android Credential Details

Get specific android credential details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier

try {
    $result = $api_instance->getOneAndroid($orgid, $projectid, $credentialid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->getOneAndroid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOneIos**
> \Swagger\Client\Model\InlineResponse20014 getOneIos($orgid, $projectid, $credentialid)

Get iOS Credential Details

Get specific iOS credential details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier

try {
    $result = $api_instance->getOneIos($orgid, $projectid, $credentialid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->getOneIos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAndroid**
> \Swagger\Client\Model\InlineResponse20013 updateAndroid($orgid, $projectid, $credentialid, $label, $file, $alias, $keypass, $storepass)

Update Android Credentials

Update an android keystore for the project. NOTE: you must be a manager in the project's organization to add new credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier
$label = "label_example"; // string | Label for the uploaded credential
$file = "/path/to/file.txt"; // \SplFileObject | Keystore file
$alias = "alias_example"; // string | Keystore alias
$keypass = "keypass_example"; // string | Keystore keypass
$storepass = "storepass_example"; // string | Keystore storepass

try {
    $result = $api_instance->updateAndroid($orgid, $projectid, $credentialid, $label, $file, $alias, $keypass, $storepass);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->updateAndroid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |
 **label** | **string**| Label for the uploaded credential | [optional]
 **file** | **\SplFileObject**| Keystore file | [optional]
 **alias** | **string**| Keystore alias | [optional]
 **keypass** | **string**| Keystore keypass | [optional]
 **storepass** | **string**| Keystore storepass | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateIos**
> \Swagger\Client\Model\InlineResponse20014 updateIos($orgid, $projectid, $credentialid, $label, $file_certificate, $file_provisioning_profile, $certificate_pass)

Update iOS Credentials

Update an iOS certificate / provisioning profile for the project. NOTE: you must be a manager in the project's organization to update credentials.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\CredentialsApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$credentialid = "credentialid_example"; // string | Credential Identifier
$label = "label_example"; // string | Label for the updated credentials
$file_certificate = "/path/to/file.txt"; // \SplFileObject | Certificate file (.p12)
$file_provisioning_profile = "/path/to/file.txt"; // \SplFileObject | Provisioning Profile (.mobileprovision)
$certificate_pass = "certificate_pass_example"; // string | Certificate (.p12) password

try {
    $result = $api_instance->updateIos($orgid, $projectid, $credentialid, $label, $file_certificate, $file_provisioning_profile, $certificate_pass);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CredentialsApi->updateIos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **credentialid** | **string**| Credential Identifier |
 **label** | **string**| Label for the updated credentials | [optional]
 **file_certificate** | **\SplFileObject**| Certificate file (.p12) | [optional]
 **file_provisioning_profile** | **\SplFileObject**| Provisioning Profile (.mobileprovision) | [optional]
 **certificate_pass** | **string**| Certificate (.p12) password | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

