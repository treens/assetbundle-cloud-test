# OrgsorgidprojectsprojectidbuildtargetsSettingsScm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**branch** | **string** |  | [optional] 
**subdirectory** | **string** | subdirectory to build from | [optional] 
**client** | **string** | perforce only client workspace to build from | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


