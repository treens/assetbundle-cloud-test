# Swagger\Client\WebhooksApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addHookForOrg**](WebhooksApi.md#addHookForOrg) | **POST** /orgs/{orgid}/hooks | Add hook for organization
[**addHookForProject**](WebhooksApi.md#addHookForProject) | **POST** /orgs/{orgid}/projects/{projectid}/hooks | Add hook for project
[**deleteHook1**](WebhooksApi.md#deleteHook1) | **DELETE** /orgs/{orgid}/hooks/{id} | Delete organization hook
[**deleteHook2**](WebhooksApi.md#deleteHook2) | **DELETE** /orgs/{orgid}/projects/{projectid}/hooks/{id} | Delete project hook
[**getHook1**](WebhooksApi.md#getHook1) | **GET** /orgs/{orgid}/hooks/{id} | Get organization hook details
[**getHook2**](WebhooksApi.md#getHook2) | **GET** /orgs/{orgid}/projects/{projectid}/hooks/{id} | Get project hook details
[**listHooksForOrg**](WebhooksApi.md#listHooksForOrg) | **GET** /orgs/{orgid}/hooks | List hooks for organization
[**listHooksForProject**](WebhooksApi.md#listHooksForProject) | **GET** /orgs/{orgid}/projects/{projectid}/hooks | List hooks for project
[**pingHook1**](WebhooksApi.md#pingHook1) | **POST** /orgs/{orgid}/hooks/{id}/ping | Ping an org hook
[**pingHook2**](WebhooksApi.md#pingHook2) | **POST** /orgs/{orgid}/projects/{projectid}/hooks/{id}/ping | Ping a project hook
[**updateHook1**](WebhooksApi.md#updateHook1) | **PUT** /orgs/{orgid}/hooks/{id} | Update hook for organization
[**updateHook2**](WebhooksApi.md#updateHook2) | **PUT** /orgs/{orgid}/projects/{projectid}/hooks/{id} | Update hook for project


# **addHookForOrg**
> \Swagger\Client\Model\InlineResponse2007 addHookForOrg($orgid, $options)

Add hook for organization

Adds a new organization level hook. An organization level hook is triggered by events from all projects belonging to the organziation. NOTE: you must be a manager in the organization to add new hooks. <h4>Hook Type Configuration Parameters</h4> <div class=\"webhook-tag-desc\"> <table> <tr><th>Type</th><th>Configuration Options</th></tr> <tr>    <td><code>web</code>    <td>       <table>          <tr><th>url</th><td>Endpoint to submit POST request</td></tr>          <tr><th>encoding</th><td>Either <code>json</code> (default) or <code>form</code></td></tr>          <tr><th>sslVerify</th><td>Verify SSL certificates of HTTPS endpoint</td></tr>          <tr><th>secret</th><td>Used to compute the SHA256 HMAC signature of the hook body and adds          a <code>X-UnityCloudBuild-Signature</code> header to the payload</td></tr>       </table>    </td> </tr> <tr>    <td><code>slack</code>    <td>       <table>          <tr><th>url</th><td>Slack incoming webhook URL. Learn more at https://api.slack.com/incoming-webhooks</td></tr>       </table>    </td> </tr> </table> </div>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$options = new \Swagger\Client\Model\Options2(); // \Swagger\Client\Model\Options2 | 

try {
    $result = $api_instance->addHookForOrg($orgid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->addHookForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **options** | [**\Swagger\Client\Model\Options2**](../Model/\Swagger\Client\Model\Options2.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addHookForProject**
> \Swagger\Client\Model\InlineResponse2007 addHookForProject($orgid, $projectid, $options)

Add hook for project

Adds a new project level hook. A project level hook is only triggered by events from the specific project. NOTE: you must be a manager in the organization to add new hooks. <h4>Hook Type Configuration Parameters</h4> <div class=\"webhook-tag-desc\"> <table> <tr><th>Type</th><th>Configuration Options</th></tr> <tr>    <td><code>web</code>    <td>       <table>          <tr><th>url</th><td>Endpoint to submit POST request</td></tr>          <tr><th>encoding</th><td>Either <code>json</code> (default) or <code>form</code></td></tr>          <tr><th>sslVerify</th><td>Verify SSL certificates of HTTPS endpoint</td></tr>          <tr><th>secret</th><td>Used to compute the SHA256 HMAC signature of the hook body and adds          a <code>X-UnityCloudBuild-Signature</code> header to the payload</td></tr>       </table>    </td> </tr> <tr>    <td><code>slack</code>    <td>       <table>          <tr><th>url</th><td>Slack incoming webhook URL. Learn more at https://api.slack.com/incoming-webhooks</td></tr>       </table>    </td> </tr> </table> </div>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$options = new \Swagger\Client\Model\Options6(); // \Swagger\Client\Model\Options6 | 

try {
    $result = $api_instance->addHookForProject($orgid, $projectid, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->addHookForProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **options** | [**\Swagger\Client\Model\Options6**](../Model/\Swagger\Client\Model\Options6.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteHook1**
> string deleteHook1($orgid, $id)

Delete organization hook

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->deleteHook1($orgid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->deleteHook1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **id** | **float**| Hook record identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteHook2**
> string deleteHook2($orgid, $projectid, $id)

Delete project hook

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->deleteHook2($orgid, $projectid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->deleteHook2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **id** | **float**| Hook record identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getHook1**
> \Swagger\Client\Model\InlineResponse2007 getHook1($orgid, $id)

Get organization hook details

Get details of a hook by id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->getHook1($orgid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->getHook1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **id** | **float**| Hook record identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getHook2**
> \Swagger\Client\Model\InlineResponse2007 getHook2($orgid, $projectid, $id)

Get project hook details

Get details of a hook by id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->getHook2($orgid, $projectid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->getHook2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **id** | **float**| Hook record identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listHooksForOrg**
> \Swagger\Client\Model\InlineResponse2007[] listHooksForOrg($orgid)

List hooks for organization

List all hooks configured for the specified organization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier

try {
    $result = $api_instance->listHooksForOrg($orgid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->listHooksForOrg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2007[]**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listHooksForProject**
> \Swagger\Client\Model\InlineResponse2007[] listHooksForProject($orgid, $projectid)

List hooks for project

List all hooks configured for the specified project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier

try {
    $result = $api_instance->listHooksForProject($orgid, $projectid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->listHooksForProject: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |

### Return type

[**\Swagger\Client\Model\InlineResponse2007[]**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pingHook1**
> string pingHook1($orgid, $id)

Ping an org hook

Send a ping event to an org hook.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->pingHook1($orgid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->pingHook1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **id** | **float**| Hook record identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pingHook2**
> string pingHook2($orgid, $projectid, $id)

Ping a project hook

Send a ping event to a project hook.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$id = 3.4; // float | Hook record identifier

try {
    $result = $api_instance->pingHook2($orgid, $projectid, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->pingHook2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **id** | **float**| Hook record identifier |

### Return type

**string**

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateHook1**
> \Swagger\Client\Model\InlineResponse2007 updateHook1($orgid, $id, $options)

Update hook for organization

Update a new hook. NOTE: you must be a manager in the organization to update hooks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$id = 3.4; // float | Hook record identifier
$options = new \Swagger\Client\Model\Options3(); // \Swagger\Client\Model\Options3 | 

try {
    $result = $api_instance->updateHook1($orgid, $id, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->updateHook1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **id** | **float**| Hook record identifier |
 **options** | [**\Swagger\Client\Model\Options3**](../Model/\Swagger\Client\Model\Options3.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateHook2**
> \Swagger\Client\Model\InlineResponse2007 updateHook2($orgid, $projectid, $id, $options)

Update hook for project

Update an existing hook. NOTE: you must be a manager of the project to update hooks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');
// Configure OAuth2 access token for authorization: permissions
Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Swagger\Client\Api\WebhooksApi();
$orgid = "orgid_example"; // string | Organization identifier
$projectid = "projectid_example"; // string | Project identifier
$id = 3.4; // float | Hook record identifier
$options = new \Swagger\Client\Model\Options7(); // \Swagger\Client\Model\Options7 | 

try {
    $result = $api_instance->updateHook2($orgid, $projectid, $id, $options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhooksApi->updateHook2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgid** | **string**| Organization identifier |
 **projectid** | **string**| Project identifier |
 **id** | **float**| Hook record identifier |
 **options** | [**\Swagger\Client\Model\Options7**](../Model/\Swagger\Client\Model\Options7.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apikey](../../README.md#apikey), [permissions](../../README.md#permissions)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

