# OrgsorgidprojectsprojectidbuildtargetsFailureDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **string** |  | [optional] 
**resolution_hint** | **string** |  | [optional] 
**stages** | [**null[]**](.md) |  | [optional] 
**failure_type** | **string** |  | [optional] 
**count** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


