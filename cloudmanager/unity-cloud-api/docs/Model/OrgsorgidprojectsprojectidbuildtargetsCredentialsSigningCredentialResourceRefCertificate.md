# OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**team_id** | **string** | generated team id from Apple | [optional] 
**cert_name** | **string** | certificate name (from the certificate) | [optional] 
**expiration** | **string** | expiration date | [optional] 
**is_distribution** | **bool** | if this is a distribution certificate | [optional] 
**uploaded** | **string** | uploaded date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


