# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**platform** | **string** |  | [optional] 
**buildtargetid** | **string** | unique id auto-generated from the build target name | [optional] 
**enabled** | **bool** | whether this target can be built by the API | [optional] 
**settings** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettings**](OrgsorgidprojectsprojectidbuildtargetsSettings.md) |  | [optional] 
**credentials** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentials**](OrgsorgidprojectsprojectidbuildtargetsCredentials.md) |  | [optional] 
**builds** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsBuilds[]**](OrgsorgidprojectsprojectidbuildtargetsBuilds.md) |  | [optional] 
**links** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


