# ProjectsSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**remote_cache_strategy** | **string** |  | [optional] [default to 'library']
**scm** | [**\Swagger\Client\Model\ProjectsSettingsScm**](ProjectsSettingsScm.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


