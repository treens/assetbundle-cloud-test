# InlineResponse20016

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** |  | [optional] 
**redirect** | **string** |  | [optional] 
**priority** | **int** |  | [optional] 
**scm_type** | **string** |  | [optional] 
**billing_plan** | **string** |  | [optional] 
**platform** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


