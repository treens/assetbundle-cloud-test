# Options3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hook_type** | **string** |  | 
**events** | **string[]** |  | [optional] 
**config** | **object** |  | 
**active** | **bool** |  | [optional] [default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


