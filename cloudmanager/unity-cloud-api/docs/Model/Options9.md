# Options9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**platform** | **string** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**settings** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettings**](OrgsorgidprojectsprojectidbuildtargetsSettings.md) |  | [optional] 
**credentials** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentials1**](OrgsorgidprojectsprojectidbuildtargetsCredentials1.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


