# OrgsorgidprojectsprojectidbuildtargetsCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**signing** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentialsSigning**](OrgsorgidprojectsprojectidbuildtargetsCredentialsSigning.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


