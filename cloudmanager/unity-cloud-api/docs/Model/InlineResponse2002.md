# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | email address | [optional] 
**name** | **string** | full name | [optional] 
**unityid** | **string** | internal unity id that is shared across services | [optional] 
**waiting** | **bool** | when true the user is waiting to be approved for access to Cloud Build | [optional] 
**disable_notifications** | **bool** | when true build status email notifications will no longer be sent | [optional] 
**api_key** | **string** | API key | [optional] 
**primary_org** | **string** | primary organization the user belongs to | [optional] 
**links** | **object** | links for retrieving more information about the user | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


