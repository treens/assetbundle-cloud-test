# Options10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**builds** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidartifactsdeleteBuilds[]**](OrgsorgidprojectsprojectidartifactsdeleteBuilds.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


