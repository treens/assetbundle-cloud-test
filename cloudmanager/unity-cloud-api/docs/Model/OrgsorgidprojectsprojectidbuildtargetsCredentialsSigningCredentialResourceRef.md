# OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platform** | **string** |  | [optional] 
**label** | **string** |  | [optional] 
**credentialid** | **string** |  | [optional] 
**created** | **string** |  | [optional] 
**last_mod** | **string** |  | [optional] 
**certificate** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefCertificate**](OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefCertificate.md) |  | [optional] 
**provisioning_profile** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefProvisioningProfile**](OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefProvisioningProfile.md) |  | [optional] 
**keystore** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefKeystore**](OrgsorgidprojectsprojectidbuildtargetsCredentialsSigningCredentialResourceRefKeystore.md) |  | [optional] 
**links** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


