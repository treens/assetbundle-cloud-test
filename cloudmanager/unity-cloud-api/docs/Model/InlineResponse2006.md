# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing** | [**\Swagger\Client\Model\InlineResponse2006Billing**](InlineResponse2006Billing.md) |  | [optional] 
**effective** | [**\Swagger\Client\Model\InlineResponse2006Effective**](InlineResponse2006Effective.md) |  | [optional] 
**billing_details** | [**\Swagger\Client\Model\InlineResponse2006BillingDetails**](InlineResponse2006BillingDetails.md) |  | [optional] 
**upgrade** | [**\Swagger\Client\Model\InlineResponse2006Upgrade**](InlineResponse2006Upgrade.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


