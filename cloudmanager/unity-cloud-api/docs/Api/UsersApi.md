# Swagger\Client\UsersApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserApiKey**](UsersApi.md#getUserApiKey) | **GET** /users/me/apikey | Get current user&#39;s API key
[**getUserSelf**](UsersApi.md#getUserSelf) | **GET** /users/me | Get current user
[**regenApiKey**](UsersApi.md#regenApiKey) | **POST** /users/me/apikey | Regenerate API Key
[**updateUserSelf**](UsersApi.md#updateUserSelf) | **PUT** /users/me | Update current user


# **getUserApiKey**
> \Swagger\Client\Model\InlineResponse2003 getUserApiKey()

Get current user's API key

Get the currently authenticated user's API key.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UsersApi();

try {
    $result = $api_instance->getUserApiKey();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getUserApiKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserSelf**
> \Swagger\Client\Model\InlineResponse2002 getUserSelf($include)

Get current user

Get the currently authenticated user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UsersApi();
$include = "include_example"; // string | Extra fields to include in the response

try {
    $result = $api_instance->getUserSelf($include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getUserSelf: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include** | **string**| Extra fields to include in the response | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **regenApiKey**
> \Swagger\Client\Model\InlineResponse2004 regenApiKey()

Regenerate API Key

Remove current API key and generate a new one. *WARNING* you will need to use the returned API key in all subsequent calls.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UsersApi();

try {
    $result = $api_instance->regenApiKey();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->regenApiKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUserSelf**
> \Swagger\Client\Model\InlineResponse2002 updateUserSelf($options)

Update current user

You can update a few fields on the current user. Each field is optional and you do not need to specify all fields on update.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UsersApi();
$options = new \Swagger\Client\Model\Options(); // \Swagger\Client\Model\Options | 

try {
    $result = $api_instance->updateUserSelf($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->updateUserSelf: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | [**\Swagger\Client\Model\Options**](../Model/\Swagger\Client\Model\Options.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

