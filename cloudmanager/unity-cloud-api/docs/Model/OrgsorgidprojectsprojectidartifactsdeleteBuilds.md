# OrgsorgidprojectsprojectidartifactsdeleteBuilds

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buildtargetid** | **string** |  | 
**build** | **float** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


