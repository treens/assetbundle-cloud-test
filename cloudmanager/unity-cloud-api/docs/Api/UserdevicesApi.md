# Swagger\Client\UserdevicesApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDevice**](UserdevicesApi.md#createDevice) | **POST** /users/me/devices | Create iOS device profile
[**listDevicesForUser**](UserdevicesApi.md#listDevicesForUser) | **GET** /users/me/devices | List iOS device profiles


# **createDevice**
> \Swagger\Client\Model\Options1 createDevice($options)

Create iOS device profile

Create iOS device profile for the current user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UserdevicesApi();
$options = new \Swagger\Client\Model\Options1(); // \Swagger\Client\Model\Options1 | 

try {
    $result = $api_instance->createDevice($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserdevicesApi->createDevice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | [**\Swagger\Client\Model\Options1**](../Model/\Swagger\Client\Model\Options1.md)|  |

### Return type

[**\Swagger\Client\Model\Options1**](../Model/Options1.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listDevicesForUser**
> \Swagger\Client\Model\InlineResponse2005[] listDevicesForUser()

List iOS device profiles

List all iOS device profiles for the current user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: apikey
Swagger\Client\Configuration::getDefaultConfiguration()->setUsername('YOUR_USERNAME');
Swagger\Client\Configuration::getDefaultConfiguration()->setPassword('YOUR_PASSWORD');

$api_instance = new Swagger\Client\Api\UserdevicesApi();

try {
    $result = $api_instance->listDevicesForUser();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserdevicesApi->listDevicesForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2005[]**](../Model/InlineResponse2005.md)

### Authorization

[apikey](../../README.md#apikey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/plain, text/html, text/csv

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

