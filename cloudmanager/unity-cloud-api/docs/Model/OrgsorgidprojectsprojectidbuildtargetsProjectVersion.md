# OrgsorgidprojectsprojectidbuildtargetsProjectVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | automatically generated name for the build | [optional] 
**filename** | **string** | filename for the primary artifact | [optional] 
**project_name** | **string** | name of the project | [optional] 
**platform** | **string** |  | [optional] 
**size** | **float** | size of the the primary build artifact in bytes | [optional] 
**created** | **string** | creation date | [optional] 
**last_mod** | **string** | last modified date | [optional] 
**bundle_id** | **string** | a unique identifier (com.example.name) | [optional] 
**udids** | **string[]** | iPhone unique identifiers that are able to install this build | [optional] 
**links** | **object** | links to build artifacts | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


