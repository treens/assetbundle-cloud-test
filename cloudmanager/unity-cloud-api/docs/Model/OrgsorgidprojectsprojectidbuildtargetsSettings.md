# OrgsorgidprojectsprojectidbuildtargetsSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_build** | **bool** | start builds automatically when your repo is updated | [optional] 
**unity_version** | **string** | &#39;latest&#39; or a unity dot version with underscores (ex. &#39;4_6_5&#39;) | [optional] 
**executablename** | **string** |  | [optional] 
**scm** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettingsScm**](OrgsorgidprojectsprojectidbuildtargetsSettingsScm.md) |  | [optional] 
**platform** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettingsPlatform**](OrgsorgidprojectsprojectidbuildtargetsSettingsPlatform.md) |  | [optional] 
**advanced** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettingsAdvanced**](OrgsorgidprojectsprojectidbuildtargetsSettingsAdvanced.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


