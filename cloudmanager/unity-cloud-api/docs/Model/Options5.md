# Options5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**settings** | [**\Swagger\Client\Model\ProjectsSettings**](ProjectsSettings.md) |  | [optional] 
**disabled** | **bool** |  | [optional] 
**disable_notifications** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


