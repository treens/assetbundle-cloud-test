# ProjectsSettingsScm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**user** | **string** |  | [optional] 
**pass** | **string** |  | [optional] 
**fingerprint** | **string** |  | [optional] 
**p4authtype** | **string** |  | [optional] 
**checkoutmethod** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


