# OrgsorgidprojectsprojectidbuildtargetsSettingsAdvanced

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xcode** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettingsAdvancedXcode**](OrgsorgidprojectsprojectidbuildtargetsSettingsAdvancedXcode.md) |  | [optional] 
**unity** | [**\Swagger\Client\Model\OrgsorgidprojectsprojectidbuildtargetsSettingsAdvancedUnity**](OrgsorgidprojectsprojectidbuildtargetsSettingsAdvancedUnity.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


