# OrgsorgidprojectsprojectidbuildtargetsTestResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unit_test** | **object** |  | [optional] 
**integration_tests** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


