# OrgsorgidprojectsprojectidcredentialssigningiosProvisioningProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**team_id** | **string** | generated team id from Apple | [optional] 
**bundle_id** | **string** | a unique identifier (com.example.name) | [optional] 
**expiration** | **string** | expiration date | [optional] 
**is_enterprise_profile** | **bool** | is this compiled for Apple&#39;s enterprise program | [optional] 
**type** | **string** |  | [optional] 
**num_devices** | **float** | number of devices provisioned for this certificate | [optional] 
**uploaded** | **string** | uploaded date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


