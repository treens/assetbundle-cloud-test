# Options11

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clean** | **bool** |  | [optional] 
**delay** | **float** |  | [optional] 
**commit** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


